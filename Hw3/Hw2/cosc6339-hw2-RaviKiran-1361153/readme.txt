How to build:
cd cosc6339-hw2-RaviKiran-1361153
ant

This will compile the classes and creates a jar file.

How to run the jobs:

There are two shell scripts present in the directory which has the commands to run each job.

Run task 1 -- sh runJob1.sh
Run task 2 -- sh runJob2.sh

Program Description:

Job1Runner.java
main -- takes input file path, output path and table name from runtime args, creates a job, set job params and launch the job.

Job2Runner.java
main -- takes tablename and output path from runtime args, creates a job, set job params and launch the job

Job1Mapper.java
extends Mapper class of the hadoop
map method, process the data recevied from file line by line, and writes to the context which is later written to the database using bulk load tool.

Job2Mapper.java
extends Mapper class of the hadoop
map method -- process the data, key value pairs are formed and set to the context.

Job2Reducer.java
extends Reducer class of the hadoop
reduce method -- reduces input obtained from the mapper and writes to the context, which is ultimatley written to the output file.

Contribution:
Ravi Krian
