package utils;

public interface Constants {

  final static byte[] INFO_COL_FAMILY = "info".getBytes();
  final static byte[] TEXT_COL_FAMILY = "text".getBytes();

}
