package reducer;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class Job2Reducer extends Reducer<Text, IntWritable, Text, DoubleWritable> {

  @Override
  protected void reduce(Text key, Iterable<IntWritable> values,
      Reducer<Text, IntWritable, Text, DoubleWritable>.Context context) throws IOException, InterruptedException {
    Double sum = 0.0;
    int size = 0;
    for (IntWritable val : values) {
      sum += val.get();
      size++;
    }
    context.write(new Text("AvgAnswers"), new DoubleWritable(sum / size));
  }
}
