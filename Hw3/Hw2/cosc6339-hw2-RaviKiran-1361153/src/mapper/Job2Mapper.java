package mapper;

import java.io.IOException;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

import utils.Constants;
import utils.HColumnEnum;

public class Job2Mapper extends TableMapper<Text, IntWritable> implements Constants {

  @Override
  protected void map(ImmutableBytesWritable key, Result values, Context context) throws IOException,
      InterruptedException {
    context.write(
        new Text("answercount"),
        new IntWritable(new Integer(new String(values.getValue(INFO_COL_FAMILY,
            HColumnEnum.INFO_ANSWERCOUNT.getColumnName())))));
  }
}
