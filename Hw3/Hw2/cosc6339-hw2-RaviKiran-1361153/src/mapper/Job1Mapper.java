package mapper;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import utils.Constants;
import utils.HColumnEnum;

public class Job1Mapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, KeyValue> implements Constants {
  ImmutableBytesWritable hKey = new ImmutableBytesWritable();
  KeyValue kv;
  private Pattern pattern =
      Pattern
          .compile("^  <row Id=\"(-?\\d+)?\".*PostTypeId=\"(-?\\d+)?\"(?:.*AcceptedAnswerId=\"(-?\\d+)?\")?(?:.*ParentId=\"(-?\\d+)?\")?.*CreationDate=\"(.*?)\".*Score=\"(-?\\d+)?\"(?:.*ViewCount=\"(-?\\d+)?\")?.*Body=\"(.*?)\"(?:.*OwnerUserId=\"(-?\\d+)?\")?(?:.*LastEditorUserId=\"(-?\\d+)?\")?(?:.*LastEditDate=\"(.*?)\")?.*LastActivityDate=\"(.*?)\"(?:.*Title=\"(.*?)\")?(?:.*Tags=\"(.*?)\")?(?:.*AnswerCount=\"(-?\\d+)?\")?(?:.*CommentCount=\"(\\d+)?\")?(?:.*CommunityOwnedDate=\"(.*?)\")?(?:.*FavoriteCount=\"(\\d+)?\")?(?:.*ClosedDate=\"(.*?)\")?.*/>$");

  @Override
  protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    Matcher matcher = pattern.matcher(value.toString());
    if (matcher.find()) {
      hKey.set(matcher.group(1).getBytes());
      if (matcher.group(2) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_POSTTYPEID.getColumnName(),
                Bytes.toBytes(matcher.group(2)));
        context.write(hKey, kv);
      }
      if (matcher.group(3) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_ACCEPTEDANSWERID.getColumnName(),
                Bytes.toBytes(matcher.group(3)));
        context.write(hKey, kv);
      }
      if (matcher.group(4) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_PARENTID.getColumnName(), Bytes.toBytes(matcher
                .group(4)));
        context.write(hKey, kv);
      }
      if (matcher.group(5) != null) {
        kv =
            new KeyValue(hKey.get(), TEXT_COL_FAMILY, HColumnEnum.TEXT_CREATIONDATE.getColumnName(),
                Bytes.toBytes(matcher.group(5)));
        context.write(hKey, kv);
      }
      if (matcher.group(6) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_SCORE.getColumnName(), Bytes.toBytes(matcher
                .group(6)));
        context.write(hKey, kv);
      }
      if (matcher.group(7) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_VIEWCOUNT.getColumnName(), Bytes.toBytes(matcher
                .group(7)));
        context.write(hKey, kv);
      }
      if (matcher.group(8) != null) {
        kv =
            new KeyValue(hKey.get(), TEXT_COL_FAMILY, HColumnEnum.TEXT_BODY.getColumnName(), Bytes.toBytes(matcher
                .group(8)));
        context.write(hKey, kv);
      }
      if (matcher.group(9) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_OWNERUSERID.getColumnName(),
                Bytes.toBytes(matcher.group(9)));
        context.write(hKey, kv);
      }
      if (matcher.group(10) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_LASTEDITUSERID.getColumnName(),
                Bytes.toBytes(matcher.group(10)));
        context.write(hKey, kv);
      }
      if (matcher.group(11) != null) {
        kv =
            new KeyValue(hKey.get(), TEXT_COL_FAMILY, HColumnEnum.TEXT_LASTEDITDATE.getColumnName(),
                Bytes.toBytes(matcher.group(11)));
        context.write(hKey, kv);
      }
      if (matcher.group(12) != null) {
        kv =
            new KeyValue(hKey.get(), TEXT_COL_FAMILY, HColumnEnum.TEXT_LASTACTDATE.getColumnName(),
                Bytes.toBytes(matcher.group(12)));
        context.write(hKey, kv);
      }
      if (matcher.group(13) != null) {
        kv =
            new KeyValue(hKey.get(), TEXT_COL_FAMILY, HColumnEnum.TEXT_TITLE.getColumnName(), Bytes.toBytes(matcher
                .group(13)));
        context.write(hKey, kv);
      }
      if (matcher.group(14) != null) {
        kv =
            new KeyValue(hKey.get(), TEXT_COL_FAMILY, HColumnEnum.TEXT_TAGS.getColumnName(), Bytes.toBytes(matcher
                .group(14)));
        context.write(hKey, kv);
      }
      if (matcher.group(15) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_ANSWERCOUNT.getColumnName(),
                Bytes.toBytes(matcher.group(15)));
        context.write(hKey, kv);
      }
      if (matcher.group(16) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_COMMENTCOUNT.getColumnName(),
                Bytes.toBytes(matcher.group(16)));
        context.write(hKey, kv);
      }
      if (matcher.group(17) != null) {
        kv =
            new KeyValue(hKey.get(), TEXT_COL_FAMILY, HColumnEnum.TEXT_COMOWNDATE.getColumnName(),
                Bytes.toBytes(matcher.group(17)));
        context.write(hKey, kv);
      }
      if (matcher.group(18) != null) {
        kv =
            new KeyValue(hKey.get(), INFO_COL_FAMILY, HColumnEnum.INFO_FAVCOUNT.getColumnName(), Bytes.toBytes(matcher
                .group(18)));
        context.write(hKey, kv);
      }
    }
  }

}
