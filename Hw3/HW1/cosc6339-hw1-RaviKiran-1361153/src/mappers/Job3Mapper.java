package mappers;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Job3Mapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {

  private Text key1 = new Text();
  private DoubleWritable value1 = new DoubleWritable(1.0);
  private Pattern pattern1 = Pattern.compile("^.*PostTypeId=\"2\".*OwnerUserId=\"(\\d+)\".*$");

  @Override
  public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    Matcher matcher = pattern1.matcher(value.toString());
    if (matcher.matches()) {
      key1.set("UserId: " + matcher.group(1));
      context.write(key1, value1);
    }
  }

}
