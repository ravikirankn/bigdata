import helper.JobRunnerHelper;

import java.io.IOException;

import mappers.Job2Mapper;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;

import reducers.Job2Reducer;


public class Job2Runner {

  @SuppressWarnings("deprecation")
  public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
    if (args.length != 2) {
      System.err.println("Usage: Job2Runner <in> <out>");
      System.exit(2);
    }
    Configuration conf = new Configuration();
    Job job = new Job(conf, "average score per accepted / unaccepted answers");
    new JobRunnerHelper().setJobParams(args[0], job, Job2Runner.class, Job2Mapper.class, Job2Reducer.class,
        Job2Reducer.class, DoubleWritable.class, args[1]);
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }

}
