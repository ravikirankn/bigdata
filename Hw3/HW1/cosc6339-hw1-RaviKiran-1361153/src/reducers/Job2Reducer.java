package reducers;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class Job2Reducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

  private DoubleWritable result = new DoubleWritable();

  @Override
  public void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException,
      InterruptedException {
    Double sum = 0.0;
    Double size = 0.0;
    for (DoubleWritable val : values) {
      sum += val.get();
      size += 1.0;
    }
    result.set(sum / size);
    context.write(key, result);
  }
}
