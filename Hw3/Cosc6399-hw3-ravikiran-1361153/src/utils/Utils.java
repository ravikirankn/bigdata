package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Utils {

  /**
   * sort the map and return a string
   * 
   * @param map
   * @return
   */
  public String sortAndReturnString(Map<String, Integer> map) {
    StringBuilder builder = new StringBuilder();
    List<Entry<String, Integer>> entries = sortByValue(map);
    for (Entry<String, Integer> t : entries) {
      builder.append(t.getKey() + ":" + t.getValue() + ",");
    }
    return builder.toString();
  }

  /**
   * sort map by value
   * 
   * @param map
   * @return
   */
  private List<Entry<String, Integer>> sortByValue(Map<String, Integer> map) {
    Comparator<Entry<String, Integer>> byValue = new Comparator<Map.Entry<String, Integer>>() {
      @Override
      public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
        if (o1.getValue() > o2.getValue()) {
          return -1;
        } else if (o1.getValue() < o2.getValue()) {
          return 1;
        } else {
          return -0;
        }
      }

    };
    List<Map.Entry<String, Integer>> entries = new ArrayList<Map.Entry<String, Integer>>();
    entries.addAll(map.entrySet());
    Collections.sort(entries, byValue);
    return entries;
  }

  /**
   * remove character at the end of the string
   * 
   * @param str
   * @return
   */
  public String removeCharAtEnd(String str) {
    if (str != null && !str.equals("")) {
      str = str.substring(0, str.length() - 1);
    }
    return str;
  }

  /**
   * get filename to store, based on the key
   * 
   * @param str
   * @return
   */
  public String getFileName(String str) {
    String charAt1 = str.substring(0, 1);
    for (FILENAME file : FILENAME.values()) {
      if (file.toString().contains(charAt1)) {
        return file.toString();
      }
    }
    return "others";
  }

  public enum FILENAME {
    abc, def, ghi, jkl, mno, pqr, stu, vwx, yz
  }
}
