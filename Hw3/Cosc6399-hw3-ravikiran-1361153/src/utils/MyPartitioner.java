package utils;

import org.apache.hadoop.mapreduce.Partitioner;

import writable.CustomWritable;

public class MyPartitioner extends Partitioner<CustomWritable, CustomWritable> {

  @Override
  public int getPartition(CustomWritable key, CustomWritable value, int paritions) {
    return key.getKey().hashCode() % paritions;
  }

}
