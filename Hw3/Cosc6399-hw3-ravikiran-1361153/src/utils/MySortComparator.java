package utils;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

import writable.CustomWritable;

public class MySortComparator extends WritableComparator {

  protected MySortComparator() {
    super(CustomWritable.class, true);
  }

  @SuppressWarnings("rawtypes")
  @Override
  public int compare(WritableComparable a, WritableComparable b) {
    CustomWritable val1 = (CustomWritable) a;
    CustomWritable val2 = (CustomWritable) b;
    int compare = val1.getKey().compareTo(val2.getKey());
    if (compare != 0) {
      return compare;
    } else {
      if (val1.getValue() < val2.getValue()) {
        return 1;
      } else if (val1.getValue() > val2.getValue()) {
        return -1;
      } else {
        return 0;
      }
    }
  }
}
