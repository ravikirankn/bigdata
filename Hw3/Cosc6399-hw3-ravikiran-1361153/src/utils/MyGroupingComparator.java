package utils;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

import writable.CustomWritable;

public class MyGroupingComparator extends WritableComparator {

  protected MyGroupingComparator() {
    super(CustomWritable.class, true);
  }

  @SuppressWarnings("rawtypes")
  @Override
  public int compare(WritableComparable a, WritableComparable b) {
    return ((CustomWritable) a).getKey().compareTo(((CustomWritable) b).getKey());
  }
}
