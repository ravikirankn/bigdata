import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utils.Utils;

public class Retrieval {

  static Utils utils = new Utils();

  public static void main(String[] args) {
    if (args.length != 2) {
      System.err.println("Usage: Retrieval <query term> <input file directory>");
      System.exit(2);
    }
    long startTime = System.nanoTime();
    String queryTerm = args[0].toLowerCase().trim();
    String fileExt = "-r-00000";
    String filePath = args[1];
    String[] queries = queryTerm.trim().split(" ");
    Map<String, String> fileVsQuery = getFileVsQueryMap(queries);
    Map<String, TreeMap<String, Integer>> listMap = new HashMap<String, TreeMap<String, Integer>>();
    for (String key : fileVsQuery.keySet()) {
      String query = fileVsQuery.get(key);
      Pattern pattern =
          Pattern
              .compile("("
                  + query
                  + ")"
                  + "\\s+(?:(.*?,))?(?:(.*?,))?(?:(.*?,))?(?:(.*?,))?(?:(.*?,))?(?:(.*?,))?(?:(.*?,))?(?:(.*?,))?(?:(.*?,))?(?:(.*?,))?");
      BufferedReader br = null;
      try {
        br = new BufferedReader(new FileReader(new File(filePath + "/" + key + fileExt)));
        String line;
        while ((line = br.readLine()) != null) {
          Matcher matcher = pattern.matcher(line);
          if (matcher.find()) {
            // System.out.println("matched: " + matcher.group(1));
            TreeMap<String, Integer> map = new TreeMap<>();
            writeResultToMap(map, matcher, 2);
            writeResultToMap(map, matcher, 3);
            writeResultToMap(map, matcher, 4);
            writeResultToMap(map, matcher, 5);
            writeResultToMap(map, matcher, 6);
            writeResultToMap(map, matcher, 7);
            writeResultToMap(map, matcher, 8);
            writeResultToMap(map, matcher, 9);
            writeResultToMap(map, matcher, 10);
            writeResultToMap(map, matcher, 11);
            listMap.put(matcher.group(1), map);
          }
        }
      } catch (IOException e) {
        System.out.println("Exception: " + e.getMessage());
      } finally {
        try {
          if (br != null) br.close();
        } catch (IOException e) {
          System.out.println("Exception: " + e.getMessage());
        }
      }
    }
    absentQueryTerms(listMap, queries);
    if (!listMap.isEmpty()) {
      // System.out.println(listMap);
      System.out.println("{" + queryTerm + "={" + utils.removeCharAtEnd(utils.sortAndReturnString(addMaps(listMap)))
          + "}}");
    }
    long endTime = System.nanoTime();
    System.out.println("Took " + (endTime - startTime) / 1000000000.0 + " secs");
  }

  private static void absentQueryTerms(Map<String, TreeMap<String, Integer>> listMap, String[] queries) {
    List<String> list = new ArrayList<String>(Arrays.asList(queries));
    if (listMap.isEmpty()) {
      System.out.println("Query terms not present: " + list);
    }
    for (String query : list) {
      if (!listMap.keySet().contains(query)) {
        System.out.println("Query term not present: " + query);
      }
    }
  }

  private static Map<String, String> getFileVsQueryMap(String[] queries) {
    Map<String, String> map = new HashMap<>();
    for (String query : queries) {
      if (query != null && !"".equalsIgnoreCase(query.trim())) {
        if (map.containsKey(utils.getFileName(query.trim()))) {
          map.put(utils.getFileName(query.trim()), map.get(utils.getFileName(query.trim())) + "|" + query.trim());
        } else {
          map.put(utils.getFileName(query.trim()), query.trim());
        }
      }
    }
    return map;
  }

  private static void writeResultToMap(Map<String, Integer> map, Matcher matcher, int i) {
    if (matcher.group(i) != null) {
      // System.out.println(matcher.group(i).trim());
      String[] splits = utils.removeCharAtEnd(matcher.group(i).trim()).split(":");
      map.put(splits[0], getInt(splits[1]));
    }
  }

  private static int getInt(String str) {
    try {
      return Integer.parseInt(str);
    } catch (Exception e) {
      return 0;
    }
  }

  private static TreeMap<String, Integer> addMaps(Map<String, TreeMap<String, Integer>> mapMap) {
    TreeMap<String, Integer> resultmap = new TreeMap<>();
    for (String key1 : mapMap.keySet()) {
      for (String key : mapMap.get(key1).keySet()) {
        if (resultmap.containsKey(key)) {
          resultmap.put(key, resultmap.get(key) + mapMap.get(key1).get(key));
        } else {
          resultmap.put(key, mapMap.get(key1).get(key));
        }
      }
    }
    return resultmap;
  }
}
