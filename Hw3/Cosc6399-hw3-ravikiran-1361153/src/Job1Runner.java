import helper.JobRunnerHelper;

import java.io.IOException;

import mapper.Job1Mapper;
import mapper.Job1SecSortMapper1;
import mapper.Job1SecSortMapper2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import reducer.Job1Reducer;
import reducer.Job1SecSortReducer1;
import reducer.Job1SecSortReducer2;
import utils.MyGroupingComparator;
import utils.MyPartitioner;
import utils.MySortComparator;
import utils.Utils.FILENAME;
import writable.CustomWritable;

public class Job1Runner {

  public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

    if (args.length < 2 || args.length > 3) {
      System.err.println("Usage: Job1Runner <in> <out1> <optional: out2>");
      System.exit(2);
    } else if (args.length == 2) {
      // with out secondary sorting
      buildInvertedIndex(args);
    } else if (args.length == 3) {
      // with secondary sorting
      buildInvertedIndexUsingSecondarySort(args);
    }

  }

  private static void buildInvertedIndexUsingSecondarySort(String[] args) throws IOException, InterruptedException,
      ClassNotFoundException {
    Configuration conf = new Configuration();
    runJob1(args, conf);
    runJob2(args, conf);
  }

  @SuppressWarnings("deprecation")
  private static void runJob1(String[] args, Configuration conf) throws IOException, InterruptedException,
      ClassNotFoundException {
    Job job1 = new Job(conf, "inverted index using secondary sort job1");
    job1.addFileToClassPath(new Path("/bigd34/jars/lucene-core-4.8.1.jar"));
    job1.addFileToClassPath(new Path("/bigd34/jars/lucene-analyzers-common-4.8.1.jar"));
    new JobRunnerHelper().setJobParams(args[0], job1, Job1Runner.class, Job1SecSortMapper1.class,
        Job1SecSortReducer1.class, null, Text.class, args[1]);
    job1.waitForCompletion(true);
    // System.exit(job1.waitForCompletion(true) ? 0 : 1);
  }

  @SuppressWarnings("deprecation")
  private static void runJob2(String[] args, Configuration conf) throws IOException, InterruptedException,
      ClassNotFoundException {
    Job job2 = new Job(conf, "inverted index using secondary sort job2");
    new JobRunnerHelper().setJobParams(args[1] + "/part-r-00000", job2, Job1Runner.class, Job1SecSortMapper2.class,
        Job1SecSortReducer2.class, null, Text.class, args[2]);
    job2.setMapOutputKeyClass(CustomWritable.class);
    job2.setMapOutputValueClass(CustomWritable.class);
    job2.setPartitionerClass(MyPartitioner.class);
    job2.setGroupingComparatorClass(MyGroupingComparator.class);
    job2.setSortComparatorClass(MySortComparator.class);
    LazyOutputFormat.setOutputFormatClass(job2, TextOutputFormat.class);
    System.exit(job2.waitForCompletion(true) ? 0 : 1);
  }

  @SuppressWarnings("deprecation")
  private static void buildInvertedIndex(String[] args) throws IOException, InterruptedException,
      ClassNotFoundException {
    Configuration conf = new Configuration();
    Job job = new Job(conf, "inverted index");
    job.addFileToClassPath(new Path("/bigd34/jars/lucene-core-4.8.1.jar"));
    job.addFileToClassPath(new Path("/bigd34/jars/lucene-analyzers-common-4.8.1.jar"));
    // job.setNumReduceTasks(3);
    new JobRunnerHelper().setJobParams(args[0], job, Job1Runner.class, Job1Mapper.class, Job1Reducer.class, null,
        Text.class, args[1]);
    for (FILENAME file : FILENAME.values()) {
      MultipleOutputs.addNamedOutput(job, file.toString(), TextOutputFormat.class, Text.class, Text.class);
    }
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
