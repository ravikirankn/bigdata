package helper;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class JobRunnerHelper {

  @SuppressWarnings({"rawtypes", "unchecked"})
  public void setJobParams(String input, Job job1, Class jarByClass, Class mapper, Class reducer, Class combiner,
      Class outPutValueClass, String output) throws IOException {
    job1.setJarByClass(jarByClass);
    job1.setMapperClass(mapper);
    if (combiner != null) {
      job1.setCombinerClass(combiner);
    }
    job1.setReducerClass(reducer);

    job1.setInputFormatClass(TextInputFormat.class);
    FileInputFormat.addInputPath(job1, new Path(input));

    FileOutputFormat.setOutputPath(job1, new Path(output));
    job1.setOutputKeyClass(Text.class);
    job1.setOutputValueClass(outPutValueClass);
  }


}
