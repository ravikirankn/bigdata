package mapper;

import java.io.IOException;
import java.io.StringReader;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseTokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

public class Job1Mapper extends Mapper<LongWritable, Text, Text, Text> {
  @Override
  protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    TokenStream tokenStream = processLine(value.toString());
    CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
    tokenStream.reset();
    while (tokenStream.incrementToken()) {
      context.write(new Text(charTermAttribute.toString()), new Text(getFileName(context)));
    }
    tokenStream.close();
  }

  private String getFileName(Context context) {
    FileSplit fileSplit = (FileSplit) context.getInputSplit();
    return fileSplit.getPath().getName();
  }

  private static TokenStream processLine(String line) throws IOException {
    TokenStream tokenStream = new StandardTokenizer(Version.LUCENE_48, new StringReader(line.trim()));
    CharArraySet stopWords = EnglishAnalyzer.getDefaultStopSet();
    // standard filter
    tokenStream = new StandardFilter(Version.LUCENE_48, tokenStream);
    // lower case tokenizer
    tokenStream = new LowerCaseTokenizer(Version.LUCENE_48, new StringReader(line.trim()));
    // filter stop words
    tokenStream = new StopFilter(Version.LUCENE_48, tokenStream, stopWords);
    // filter non ascii chars
    tokenStream = new ASCIIFoldingFilter(tokenStream);
    return tokenStream;
  }

}
