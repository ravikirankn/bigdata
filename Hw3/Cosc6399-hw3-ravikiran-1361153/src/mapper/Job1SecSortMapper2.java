package mapper;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import writable.CustomWritable;

public class Job1SecSortMapper2 extends Mapper<LongWritable, Text, CustomWritable, CustomWritable> {

  @Override
  protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    StringTokenizer tokenizer1 = new StringTokenizer(value.toString());
    while (tokenizer1.hasMoreTokens()) {
      String word = tokenizer1.nextToken();
      StringTokenizer tokenizer2 = new StringTokenizer(tokenizer1.nextToken().toString(), ":");
      String fileName = tokenizer2.nextToken();
      String count = tokenizer2.nextToken();
      context.write(new CustomWritable(word, getInt(count)), new CustomWritable(fileName, getInt(count)));
    }
  }

  private static int getInt(String str) {
    try {
      return Integer.parseInt(str);
    } catch (Exception e) {
      return 0;
    }
  }

}
