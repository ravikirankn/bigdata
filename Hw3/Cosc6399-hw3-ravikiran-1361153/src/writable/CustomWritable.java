package writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

public class CustomWritable implements WritableComparable<CustomWritable> {
  private String key;
  private int value;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public CustomWritable() {
    // TODO Auto-generated constructor stub
  }

  public CustomWritable(String key, int value) {
    this.key = key;
    this.value = value;
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    key = WritableUtils.readString(in);
    value = in.readInt();
  }

  @Override
  public void write(DataOutput out) throws IOException {
    WritableUtils.writeString(out, key);
    out.writeInt(value);
  }

  @Override
  public int compareTo(CustomWritable o) {
    int compare = key.compareTo(o.key);
    if (compare != 0) {
      return compare;
    } else {
      if (value < o.value) {
        return -1;
      } else if (value > o.value) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  @Override
  public String toString() {
    return (new StringBuilder()).append(value).append(' ').toString();
  }

}
