package reducer;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class Job1SecSortReducer1 extends Reducer<Text, Text, Text, Text> {

  @Override
  protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
    int count = 0;
    for (Text val : values) {
      count++;
    }
    String[] split = key.toString().split(",");
    context.write(new Text(split[0]), new Text(split[1] + ":" + count));
  }
  
}
