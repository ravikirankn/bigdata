package reducer;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import utils.Utils;
import writable.CustomWritable;

public class Job1SecSortReducer2 extends Reducer<CustomWritable, CustomWritable, Text, Text> {

  private MultipleOutputs<Text, Text> mos;


  @Override
  protected void setup(Context context) throws IOException, InterruptedException {
    mos = new MultipleOutputs<Text, Text>(context);
  }

  @Override
  protected void reduce(CustomWritable key, Iterable<CustomWritable> values, Context arg2) throws IOException,
      InterruptedException {
    StringBuilder builder = new StringBuilder();
    for (CustomWritable val : values) {
      builder.append(val.getKey() + ":" + val.getValue() + ",");
    }
    mos.write(new Text(key.getKey()), new Text(builder.toString()), new Utils().getFileName(key.getKey()));
  }

  @Override
  public void cleanup(Context context) throws IOException, InterruptedException {
    mos.close();
  }
}
