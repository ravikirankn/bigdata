package reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import utils.Utils;

public class Job1Reducer extends Reducer<Text, Text, Text, Text> {

  private MultipleOutputs<Text, Text> mos;

  @Override
  protected void setup(Context context) throws IOException, InterruptedException {
    mos = new MultipleOutputs<Text, Text>(context);
  }

  @Override
  protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
    Map<String, Integer> map = new HashMap<String, Integer>();
    for (Text val : values) {
      if (map.containsKey(val.toString())) {
        map.put(val.toString(), map.get(val.toString()) + 1);
      } else {
        map.put(val.toString(), 1);
      }
    }
    // context.write(key, new Text((new Utils()).sortAndReturnString(map)));
    mos.write(key, new Text((new Utils()).sortAndReturnString(map)), new Utils().getFileName(key.toString()));
  }

  @Override
  public void cleanup(Context context) throws IOException, InterruptedException {
    mos.close();
  }

}
