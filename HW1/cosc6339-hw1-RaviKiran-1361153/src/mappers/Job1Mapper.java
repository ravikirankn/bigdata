package mappers;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Job1Mapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {

  private Text key1 = new Text("AnswerCountList");
  private DoubleWritable value1 = new DoubleWritable();
  private Pattern pattern1 = Pattern.compile("^  <row Id=\"(\\d+)\" PostTypeId=\"1\".*AnswerCount=\"(\\d+)\".*$");

  @Override
  public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    Matcher matcher = pattern1.matcher(value.toString());
    if (matcher.matches()) {
      value1.set(getDoubleValue(matcher.group(2)));
      context.write(key1, value1);
    }
  }

  private Double getDoubleValue(String value) {
    try {
      return Double.valueOf(value.trim());
    } catch (Exception e) {
      return 0.0;
    }
  }

}
