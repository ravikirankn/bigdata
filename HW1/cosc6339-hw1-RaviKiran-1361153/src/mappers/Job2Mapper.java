package mappers;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Job2Mapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {

  private Set<String> ids = new HashSet<>();
  private Text key1 = new Text();
  private DoubleWritable value1 = new DoubleWritable();
  public Pattern pattern1 = Pattern
      .compile("^.*<row Id=\"(\\d+)\" PostTypeId=\"(\\d+)\" (?:AcceptedAnswerId=\"(\\d+)\")?.*$");
  public Pattern pattern2 = Pattern.compile("^.*<row Id=\"(\\d+)\".*Score=\"(-?\\d+)\".*$");

  @Override
  protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    Matcher matcher = pattern1.matcher(value.toString());
    if (matcher.matches()) {
      ids.add(matcher.group(3));
      if (ids.remove(trim(matcher.group(1))) && trim(matcher.group(2)).equalsIgnoreCase("2")) {
        Matcher matcher1 = pattern2.matcher(value.toString());
        if (matcher1.matches()) {
          key1.set("Accepted: ");
          value1.set(getDoubleValue(matcher1.group(2)));
          context.write(key1, value1);
        }
        context.write(key1, value1);
      } else if (trim(matcher.group(2)).equalsIgnoreCase("2")) {
        Matcher matcher1 = pattern2.matcher(value.toString());
        if (matcher1.matches()) {
          key1.set("NotAccepted: ");
          value1.set(getDoubleValue(matcher1.group(2)));
          context.write(key1, value1);
        }
      }
    }
  }

  private Double getDoubleValue(String value) {
    try {
      return Double.valueOf(value);
    } catch (Exception e) {
      return 0.0;
    }
  }

  private String trim(String string) {
    if (string != null) {
      return string.trim();
    } else {
      return "";
    }
  }

}
