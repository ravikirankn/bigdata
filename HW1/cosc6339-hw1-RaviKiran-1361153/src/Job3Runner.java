import helper.JobRunnerHelper;

import java.io.IOException;

import mappers.Job3Mapper;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;

import reducers.Job3Reducer;

public class Job3Runner {

  @SuppressWarnings("deprecation")
  public static void main(String[] args) throws IllegalArgumentException, IOException, ClassNotFoundException,
      InterruptedException {
    if (args.length != 2) {
      System.err.println("Usage: Job3Runner <in> <out>");
      System.exit(2);
    }
    Configuration conf = new Configuration();
    Job job = new Job(conf, "average answers per user id");
    new JobRunnerHelper().setJobParams(args[0], job, Job3Runner.class, Job3Mapper.class, Job3Reducer.class,
      Job3Reducer.class, DoubleWritable.class, args[1]);
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }

}
