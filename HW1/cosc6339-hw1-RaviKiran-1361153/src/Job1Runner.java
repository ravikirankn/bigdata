import helper.JobRunnerHelper;

import java.io.IOException;

import mappers.Job1Mapper;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;

import reducers.Job1Reducer;

public class Job1Runner {

  @SuppressWarnings("deprecation")
  public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
    if (args.length != 2) {
      System.err.println("Usage: Job1Runner <in> <out>");
      System.exit(2);
    }
    Configuration conf = new Configuration();
    Job job = new Job(conf, "average number of posts");
    new JobRunnerHelper().setJobParams(args[0], job, Job1Runner.class, Job1Mapper.class, Job1Reducer.class,
        Job1Reducer.class, DoubleWritable.class, args[1]);
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }

}
