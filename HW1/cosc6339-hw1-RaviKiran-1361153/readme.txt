How to build:
cd cosc6339-hw1-RaviKiran-1361153
ant

This will compile the classes and creates a jar file.

How to run the jobs:

There are three shell scripts present in the directory which has the commands to run each job.

Run task 1 -- sh runJob1.sh
Run task 2 -- sh runJob2.sh
Run task 3 -- sh runJob3.sh 

Program Description:

JobRunnerHelper.java --
setJobParams method -- takes the reference of the job and input params like reducer class,
mapper class, output values type etc. and sets it to the job.
since there are multiple jobs and this part of the code is repetitive this is seperated from the job runners.

Job1Runner.java, Job2Runner.java, Job3Runner.java
main -- takes input file path and output path from runtime args, creates a job, set job params and launch the job.

Job1Mapper.java, Job2Mapper.java, Job3Mapper.java
extends Mapper class of the hadoop
map method -- process the data, key value pairs are formed and set to the context.

Job1Reducer.java, Job2Reducer.java, Job3Reducer.java
extends Reducer class of the hadoop
reduce method -- reduces input obtained from the mapper and writed to the context which is ultimatley written to the output file.

Contribution:
Ravi Krian
