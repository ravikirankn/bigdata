package utils;

public enum HColumnEnum {
  
  INFO_POSTTYPEID ("PostTypeId".getBytes()),
  INFO_ACCEPTEDANSWERID ("AcceptedAnswerId".getBytes()),
  INFO_OWNERUSERID ("OwnerUserId".getBytes()),
  INFO_SCORE ("Score".getBytes()),
  INFO_VIEWCOUNT ("ViewCount".getBytes()),
  INFO_ANSWERCOUNT ("AnswerCount".getBytes()),
  INFO_COMMENTCOUNT ("CommentCount".getBytes()),
  INFO_PARENTID ("ParentId".getBytes()),
  INFO_LASTEDITUSERID ("LastEditorUserId".getBytes()),
  INFO_FAVCOUNT ("FavoriteCount".getBytes()),
  TEXT_BODY ("Body".getBytes()),
  TEXT_TITLE ("Title".getBytes()),
  TEXT_TAGS ("Tags".getBytes()),
  TEXT_CREATIONDATE ("CreationDate".getBytes()),
  TEXT_LASTEDITDATE ("LastEditDate".getBytes()),
  TEXT_LASTACTDATE ("LastActivityDate".getBytes()),
  TEXT_COMOWNDATE ("CommunityOwnedDate".getBytes());
 
  private final byte[] columnName;
  
  HColumnEnum (byte[] column) {
    this.columnName = column;
  }

  public byte[] getColumnName() {
    return this.columnName;
  }
  
}
