import mapper.Job2Mapper;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import reducer.Job2Reducer;
import utils.Constants;
import utils.HColumnEnum;

public class Job2Runner implements Constants {

  public static void main(String[] args) throws Exception {
    if (args.length != 2) {
      System.err.println("Usage: Job2Runner <HbaseTableName> <outputFile>");
      System.exit(2);
    }
    Configuration conf = HBaseConfiguration.create();
    Job job = new Job(conf, "AvgAnswers");
    job.setJarByClass(Job2Runner.class);
    Scan scan = new Scan();
    scan.setCaching(500);
    scan.setCacheBlocks(false);
    setColumnFamily(scan);
    setFilters(scan);
    TableMapReduceUtil.initTableMapperJob(args[0], scan, Job2Mapper.class, Text.class, IntWritable.class, job);
    job.setReducerClass(Job2Reducer.class);
    job.setNumReduceTasks(1);
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }

  private static void setColumnFamily(Scan scan) {
    scan.addFamily(INFO_COL_FAMILY);
    scan.addColumn(INFO_COL_FAMILY, HColumnEnum.INFO_ANSWERCOUNT.getColumnName());
  }

  private static void setFilters(Scan scan) {
    FilterList li = new FilterList(FilterList.Operator.MUST_PASS_ALL);
    SingleColumnValueFilter filter =
        new SingleColumnValueFilter(INFO_COL_FAMILY, HColumnEnum.INFO_POSTTYPEID.getColumnName(), CompareOp.EQUAL,
            Bytes.toBytes("1"));
    li.addFilter(filter);
    scan.setFilter(li);
  }
}
